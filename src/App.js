import React, { Component } from 'react';
import Buscador             from './components/Buscador'
import Catalogo             from './components/Catalogo';
import './styles/cargando.css';
// http://tobiasahlin.com/spinkit/

class App extends Component {

  state = {
    termino:        '',
    imagenes:       [],
    pagina:         '',
    cargando:       false,
    total_paginas:  ''
  }

  consultarAPI = async ( ) => {
    const KEY     = '10547590-561862cf80d7e34284c5268bc';
    const termino = this.state.termino;
    const pagina = this.state.pagina;
    const URL     = 'https://pixabay.com/api/?key=' + KEY + '&q=' + termino + '&per_page=30&page=' + pagina;
    await fetch( URL )
      .then(res => {
        this.setState( {cargando: true} );
        return res.json();
      } )
      .then( res => {
        // console.log(res.totalHits)
        // return setTimeout( () => {
          this.setState( {
            imagenes:       res.hits,
            cargando:       false,
            total_paginas:  Math.ceil( res.totalHits / 30 )
          } );
        // }, 2000 )
      } )
  }

  getDatosBusqueda = (termino) => {
    this.setState( 
      {termino, pagina: 1}, () => {
        this.consultarAPI();
      }
    );
  }

  paginaAnterior = () => {
    if( this.state.pagina === 1 ) return;
    this.setState( { pagina: this.state.pagina -1 }, () => {
      this.consultarAPI();
      this.scrollCatalogo();
    } );
  }
  paginaSiguiente = () => {
    if( this.state.pagina === this.state.total_paginas ) return;
    this.setState( { pagina: this.state.pagina +1 }, () => {
      this.consultarAPI();
      this.scrollCatalogo();
    } );
  }

  scrollCatalogo = () => {
    const catalogo = document.querySelector('#buscador');
    catalogo.scrollIntoView( 'auto', 'start' );
  }

  render() {
    const cargando = this.state.cargando;
    let resultado;
    if( cargando ){
      resultado = <div className="spinner">
                    <div className="cube1"></div>
                    <div className="cube2"></div>
                  </div>
    }
    else{
      resultado = <Catalogo 
                      imagenes={this.state.imagenes}
                      paginaAnterior={this.paginaAnterior}
                      paginaSiguiente={this.paginaSiguiente}
                      pagina={this.state.pagina}
                      total_paginas={this.state.total_paginas} />
    }
    return (
      <div className="app container">
        <div className='jumbotron'>
          <p className='lead text-center'> Buscador de imágenes </p>
          <Buscador
              getDatosBusqueda={this.getDatosBusqueda}/>
        </div>
        <div className='row justify-content-center'>
          {resultado}
        </div>
      </div>
    );
  }
}

export default App;
