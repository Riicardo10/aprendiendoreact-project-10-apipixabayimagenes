import React from 'react';

const Imagen = (props) => {
    let {id, largeImageURL, likes, previewURL, tags, views} = props.imagen;
    return (
        <div className='col-12 col-sm-6 col-md-4 col-lg-3 mb-3'>
            <div className='card'>
                <img src={previewURL} className='card-img-top' alt={tags} title={tags} />
                <div className='card-body'>
                    <p className='car-text'> {likes} Me gusta </p>
                    <p className='car-text'> {views} Vistas </p>
                    <a href={largeImageURL} target='_blank' className='btn btn-primary btn-block'> Ver imagen </a>
                </div>
            </div>
        </div>
    );
}

export default Imagen;