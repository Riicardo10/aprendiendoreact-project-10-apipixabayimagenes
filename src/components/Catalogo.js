import React, { Component } from 'react';
import Imagen from './Imagen';
import Paginacion from './Paginacion';

class Catalogo extends Component {

    getCatalogo = () => {
        if( !this.props.imagenes )              return null;
        if( this.props.imagenes.length === 0 )  return null;
        let imagenes = this.props.imagenes;
        return (
            <React.Fragment>
                <div id='catalogo' className='col-12 p-5 row'>
                    {imagenes.map( imagen => {
                        return <Imagen imagen={imagen} key={imagen.id} />
                    } ) }
                </div>
                <div className='row justify-content-center'>
                    <Paginacion
                        paginaAnterior={this.props.paginaAnterior}
                        paginaSiguiente={this.props.paginaSiguiente}
                        pagina={this.props.pagina}
                        total_paginas={this.props.total_paginas} />
                </div>
            </React.Fragment>
        );
    }

    render() {
        return (
            <div>
                {this.getCatalogo()}
            </div>
        );
    }
}

export default Catalogo;