import React, { Component } from 'react';

class Buscador extends Component {

    busqueda = React.createRef();

    getDatosForm = (e) => {
        e.preventDefault();
        
        let busqueda = this.busqueda.current.value;
        this.props.getDatosBusqueda( busqueda );
    }

    render() {
        return (
            <form onSubmit={this.getDatosForm}>
                <div id='buscador' className='row'>
                    <div className='form-group col-md-8'>
                        <input ref={this.busqueda} className='form-control form-control-lg' type='text' placeholder='Buscar: Ej. futbol' />
                    </div>
                    <div className='form-group col-md-4'>
                        <input type='submit' className='btn btn-success btn-block btn-lg' value='Buscar' />
                    </div>
                </div>
            </form>
        );
    }
}

export default Buscador;