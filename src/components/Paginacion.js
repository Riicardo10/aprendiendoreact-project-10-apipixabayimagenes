import React, {Component} from 'react';

class Paginacion extends Component {

    showAnterior = () => {
        if( this.props.pagina === 1 ) return null;
        return <button onClick={ () => this.props.paginaAnterior() } className='btn btn-info mr-1'> &larr; Anterior </button>
    }
    showSiguiente = () => {
        if( this.props.pagina === this.props.total_paginas ) return null;
        return <button onClick={ () => this.props.paginaSiguiente() } className='btn btn-info'> Siguiente &rarr; </button>
    }

    render() {
        return (
            <div className='py-5'>
                { this.showAnterior() }
                { this.showSiguiente() }
            </div>
        );
    }
}

export default Paginacion;